# covid.py

Quick and dirty script to extract province-level COVID data from the Canadian government's [interactive COVID-19 data visualization page](https://health-infobase.canada.ca/covid-19/).

## Requirements

Python 3.7+ or so

## Usage

```
./covid.py <province>
```

## Notes

I use this tool to get a general sense of how things are going, not to do real science. I am not an authority using this tool to make public policy decisions, and neither should you. I could not find any documentation on this data source, which means there are some untested assumptions in this script on the meanings of the data fields.

I know the data from this on Manitoba looks different from the provincial data made available on their [dashboard](https://experience.arcgis.com/experience/f55693e56018406ebbd08b3492e99771), although the general shape appears the same. I don't think this is a big deal, and could boil down to things like logging new cases based on test date vs result date, or provincial data getting sent to the federal government in batches instead of as a stream. IDK!
