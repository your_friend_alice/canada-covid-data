#!/usr/bin/env python3
import http.client, csv, sys
from datetime import datetime

host = 'health-infobase.canada.ca'
path = '/src/data/covidLive/covid19.csv'

todayFields = {
        'numtestedtoday': 'tested',
        'numtoday': 'confirmed',
        'numrecoveredtoday': 'recovered',
        'numdeathstoday': 'dead',
        }

totalFields = {
        'numtested': 'tested',
        'numprob': 'probable',
        'numconf': 'confirmed',
        'numrecover': 'recovered',
        'numdeaths': 'dead',
        'numtotal': 'total',
        }

def parseRow(row, mapping):
    return {
        outKey: int(float(row[rowKey]))
        for rowKey, outKey in mapping.items()
        if rowKey in mapping and row[rowKey]
        }

def cases(region):
    conn = http.client.HTTPSConnection(host)
    conn.request('GET', path, headers={'Accept': 'text/csv'})
    response = conn.getresponse()
    for row in csv.DictReader(line.decode() for line in response):
        if row['prname'].lower() == region.lower() or row['prnameFR'].lower() == region.lower():
            today = parseRow(row, todayFields)
            total = parseRow(row, totalFields)
            yield {
                    'date': datetime.strptime(row['date'], '%d-%m-%Y').date(),
                    'today': {'active': total.get('probable', 0) + total.get('confirmed', 0) - total.get('recovered', 0) - total.get('dead', 0), **today},
                    'total': total
                    }

highDead = 0
highNew = 0
highActive = 0
for row in cases(sys.argv[1]):
    records = []
    if row['today']['confirmed'] > highNew:
        highNew = row['today']['confirmed']
        records.append("new")
    if row['today']['dead'] > highDead:
        highDead = row['today']['dead']
        records.append("dead")
    if row['today']['active'] > highActive:
        highActive = row['today']['active']
        records.append("active")
    if row['date'].weekday() == 6:
        print('\033[1m', end='')
    print('{}: New: {: >4}, Active: {: >4}, Dead: {: >2}, Total Dead: {: >3}'.format(
        datetime.strftime(row['date'], "%a %Y-%m-%d"),
        row['today']['confirmed'],
        row['today']['active'],
        row['today']['dead'],
        row['total']['dead']
        ) + ((" Records: " + ",".join(records)) if len(records) > 0 else ""))
    if row['date'].weekday() == 6:
        print('\033[0m', end='')
